// content.js
// When a user clicks on the eye icon this script will find all instances of the male pronouns and replace
// them with those chosen by the user. If the user has not chosen their pronouns then female pronouns will be
// applied by default
// TODO: 
//   Fix bug with Dependent versus independent pronouns 
//   Clean up UI/UX - not obvious how/where to set preferred pronouns, options page is basic
//   Consider adding more formal tests
//   Extend to include option to swap out male identifiers such as male, man, boy, brother, father, son etc.
chrome.runtime.onMessage.addListener(
    function(request, sender, sendResponse) {

        function upperFirstLetter(string) {
            return string.charAt(0).toUpperCase() + string.slice(1);
        }
        function lowerFirstLetter(string) {
            return string.charAt(0).toLowerCase() + string.slice(1);
        }
        // words to swap out
        var oldWords = ["he", "him", "his", "himself","He", "Him", "His", "Himself"];
        // default replacement words - use these pronouns if user has not set their own yet
        var defaultNewWords = ["she", "her", "her", "herself","She", "Her", "Her", "Herself"]
        
        if( request.message === "clicked_browser_action" ) {
		      // get the user options from storage for their chosen pronouns
	  		   chrome.storage.sync.get(['pron1','pron2','pron3','pron4'], function(result) {

	     			var newWords = [
	            result.pron1==''||result.pron1===undefined? defaultNewWords[0]:lowerFirstLetter(result.pron1),
	            result.pron2==''||result.pron2===undefined? defaultNewWords[1]:lowerFirstLetter(result.pron2),
	            result.pron3==''||result.pron3===undefined? defaultNewWords[2]:lowerFirstLetter(result.pron3),
	            result.pron4==''||result.pron4===undefined? defaultNewWords[3]:lowerFirstLetter(result.pron4),
	            result.pron1==''||result.pron1===undefined? defaultNewWords[4]:upperFirstLetter(result.pron1),
	            result.pron2==''||result.pron2===undefined? defaultNewWords[5]:upperFirstLetter(result.pron2),
	            result.pron3==''||result.pron3===undefined? defaultNewWords[6]:upperFirstLetter(result.pron3),
	            result.pron4==''||result.pron4===undefined? defaultNewWords[7]:upperFirstLetter(result.pron4),
	            ];

	            // for each key value pair perform the replacement
	            // replace on lowercase version
	            for (var i = 0, len = oldWords.length; i < len; i++) {

						// make sure we are only replacing standalone words (e.g. don't want to replace the "he" in "hello")
						var re = new RegExp(`\\b${oldWords[i]}\\b`, 'g');

	               // props to padolsey and his find-replace code over at https://github.com/padolsey/findAndReplaceDOMText
						findAndReplaceDOMText(document.getElementsByTagName("body")[0], { 
							preset: 'prose',
							find: re,
							replace: newWords[i]});
	            }
	        });  
        }
    }
);	
