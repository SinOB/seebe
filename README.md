# SeeBe

"You can't be what you can't see," - Marian Wright Edelman, American civil rights activist.

This is a very basic WIP but functional Chrome Extension to allow users to replace male pronouns 
on a web page with their own chosen pronouns.

## Installation

1. Download this repository to your local machine.

2. In the URL bar go to 
```
chrome://extensions/
```

3. Click on the button "Load unpacked".

4. In the window that pops up navigate to the folder where you have downloaded this repo and click open.

5. If successfully installed you should then see an eye logo in your chrome menu bar. 

6. Go about your business. When you get to a page where it's a bit male/pale/stale click on the eye icon to update the page with your chosen pronouns. (The tool is set to use female pronouns by default but it is possible to change these in the extension settings)

## Usage
Don't be a tool when using this tool. No Natzies, alt-righters, racists, haters or any *ists at all.

## License
[DBAD](https://dbad-license.org)