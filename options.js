// Saves options to chrome.storage
function save_options() {

  var pron1 = document.getElementById('pron1').value;
  var pron2 = document.getElementById('pron2').value;
  var pron3 = document.getElementById('pron3').value;
  var pron4 = document.getElementById('pron4').value;

  if(pron1 =='' || pron2=='' || pron3 ==''|| pron4==''){
		window.alert("Each of the form fields is required. Please enter a string");
		return;
  }

  chrome.storage.sync.set({
    pron1: pron1,
    pron2: pron2,
    pron3: pron3,
    pron4: pron4
  }, function() {
    // Update status to let user know options were saved.
    var status = document.getElementById('status');
    status.textContent = 'Options saved.';
    setTimeout(function() {
      status.textContent = '';
    }, 750);
  });
}

// Restores select box and checkbox state using the preferences
// stored in chrome.storage.
function restore_options() {
  // Use default values.
  chrome.storage.sync.get({
    pron1: 'she',
    pron2: 'hers',
    pron3: 'her',
    pron4: 'herself'
  }, function(items) {
    document.getElementById('pron1').value = items.pron1;
    document.getElementById('pron2').value = items.pron2;
    document.getElementById('pron3').value = items.pron3;
    document.getElementById('pron4').value = items.pron4;
  });
}
document.addEventListener('DOMContentLoaded', restore_options);
document.getElementById('save').addEventListener('click',
    save_options);